##What You'll Need On Your Host Machine
* Access to PHP from your command line (with the open SSL library enabled)
* Composer
* Vagrant
* VirtualBox

##Project Installation
* Clone this repository into an empty directory
* `CD` into project directory
* Execute `composer install`
* Execute the "make" command fom your project root
   
    * _Note: This will generate your Homestead.yaml and Vagrantfile files._

    * Linux:   `php vendor/bin/homestead make`
    
    * Windows: `vendor\\bin\\homestead make`

    * After you've generated your Homestead.yaml, be sure to look it over to confirm that all VM settings are as expected. 
 **_Note: You may want to update the application address before executing the next step. 
 The default development address will be: econtracting.dev.app_**
    
* Execute `vagrant up`

* Vagrant SSH (SSH into th VM)

* `CD /home/vagrant/Code/econtracting-dev` (from inside the VM)

* Execute `php artisan migrate` from the project's root directory to install the application's data tables (from inside the VM)

##Documentation References
* More about Laravel's Homestead VM: https://laravel.com/docs/5.4/homestead

* More about Laravel: https://laravel.com/

* More about Vagrant: https://www.vagrantup.com/intro/index.html

* More about VirtualBox: https://www.virtualbox.org/wiki/VirtualBox