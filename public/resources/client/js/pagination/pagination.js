/**
 * @author Charles Moss
 */
var pagination = (function($, undefined) {

    var containers;
    var containerSelector   = '.pagination-container';
    var globalCallbacks     = [];
    var containerCallbacks  = {};
    var fetchXHR            = null;

    function loadContainer(container)
    {
        var alias   = container.attr('data-alias'),
            page    = container.attr('data-page'),
            perPage = container.attr('data-per-page'),
            address = container.attr('data-address'),
            filters = container.find('.filter-inputs').serialize();

        if (address === undefined || alias === undefined || page === undefined) {
            console.log('A pagination container failed to load because not all required data attributes are present.');
            return;
        }

        if (filters === undefined || filters == '') {
            filters = 'page=' + page
        } else {
            filters += '&page=' + page
        }

        if (perPage !== undefined) {
            filters += '&per-page=' + perPage
        }

        //Show loading indicator
        container.find('.loading-indicator').removeClass('hide');
        //Hide dynamic data
        container.find('.dynamic-html').addClass('hide');

        if (fetchXHR !== null) {
            fetchXHR.abort();
        }

        fetchXHR = $.get(address, filters, function(response) {

            //Hide loading indicator
            container.find('.loading-indicator').addClass('hide');

            //Show dynamic data
            container.find('.dynamic-html').removeClass('hide');

            //Fill the content section inside the container
            container.find('.dynamic-html').html(response);

            //Call global callbacks if any are present
            if (globalCallbacks.constructor === Array) {
                $.each(globalCallbacks, function(key, callback) {
                    if (typeof callback == 'function') {
                        callback(response);
                    } else {
                        console.log('A pagination callback failed to execute because an invalid value was provided.')
                    }
                });
            }

            //Call container callbacks if any are present
            if (containerCallbacks[alias] !== undefined && containerCallbacks[alias].constructor === Array) {
                $.each(containerCallbacks[alias], function(key, callback) {
                    if (typeof callback == 'function') {
                        callback(response);
                    } else {
                        console.log('A pagination callback failed to execute because an invalid value was provided.')
                    }
                });
            }

        });
    }

    function containerNextPage()
    {
        var button = $(this),
            container = button.closest(containerSelector),
            currentContainerPage = parseInt(container.attr('data-page'));

        if (currentContainerPage === undefined) {
            currentContainerPage = 1;
        } else {
            currentContainerPage = parseInt(currentContainerPage);
        }

        container.attr('data-page', currentContainerPage + 1);
        loadContainer(container);
    }

    function containerLastPage()
    {
        var button = $(this),
            container = button.closest(containerSelector),
            currentContainerPage = container.attr('data-page');

        if (currentContainerPage === undefined) {
            currentContainerPage = 1;
        } else {
            currentContainerPage = parseInt(currentContainerPage);
        }

        if (currentContainerPage > 1) {
            container.attr('data-page', currentContainerPage - 1);
            loadContainer(container);
        }
    }

    function containerSearch()
    {
        var input = $(this),
            container = input.closest(containerSelector);

        container.attr('data-page', 1);
        loadContainer($(this).closest(containerSelector));
    }

    function containerRefresh(alias)
    {
        var container = $(containerSelector + "[data-alias='" + alias + "']");

        if (!container.length) {
            console.log('Failed to refresh pagination, you may have provided an invalid alias.');
            return;
        }

        loadContainer(container);
    }

    function applyContainerListeners(contentContainer)
    {
        contentContainer.on('click', containerSelector + ' .next-page', containerNextPage);
        contentContainer.on('click', containerSelector + ' .last-page', containerLastPage);
        contentContainer.on(
            'change',
            containerSelector + ' .filter-inputs input,' + containerSelector + ' .filter-inputs select',
            containerSearch
        );
        contentContainer.on('submit', containerSelector + ' form.filter-inputs', function(e) {
            e.preventDefault();
        });
    }

    function initialize()
    {
        containers = $(containerSelector);
        applyContainerListeners($('body'));
        $.each(containers, function(key, value) {
            loadContainer($(value));
        });
    }

    return {
        init: initialize,
        refresh: containerRefresh,
        afterFill: function(alias, callback)
        {
            if (typeof alias == 'function') {
                globalCallbacks.push(callback);
                return;
            }

            if (containerCallbacks[alias] === undefined) {
                containerCallbacks[alias] = [];
            }

            containerCallbacks[alias].push(callback);
        }
    }

})(jQuery);