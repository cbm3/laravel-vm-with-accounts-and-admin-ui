/**
 * @author Charles Moss
 */
(function($, undefined) {

    var body = $('body'),
        modal = $('#md-modal');

    body.on('click', '.add-license', function(e) {

        var self = $(this);

        e.preventDefault();

        modal.find(".modal-content").load(self.attr('href'), function() {
            modal.find('.date-picker').datepicker();
            modal.modal('show');
        });

    });

    body.on('click', '.review-license', function(e) {

        var self = $(this);

        e.preventDefault();

        modal.find(".modal-content").load(self.attr('href'), function() {
            modal.find('.date-picker').datepicker();
            modal.modal('show');
        });

    });

    body.on('click', '.review-license-image', function(e) {

        var self = $(this);

        e.preventDefault();

        if (self.hasClass('disabled')) {
            return false;
        }

        modal.find(".modal-content").load(self.attr('href'), function() {
            modal.modal('show');
        });

    });

    body.on('click', '.delete-image', function(e) {

        var self = $(this);

        e.preventDefault();

        confirm(self.data('confirm-message'), self.data('confirm-button'), 'warning', function() {
            $.post(self.attr('href'), {}, function(response) {
                notify(response);
                pagination.refresh('license-listing');
                modal.modal('hide');
            });
        });

    });

    body.on('click', '.delete-license', function(e) {

        var self = $(this);

        e.preventDefault();

        confirm(self.data('confirm-message'), self.data('confirm-button'), 'warning', function() {
            $.post(self.attr('href'), {}, function(response) {
                notify(response);
                pagination.refresh('license-listing');
            });
        });

    });

    body.on('submit', 'form#add-license', function(e) {

        e.preventDefault();

        var self = $(this);
        confirm(self.data('confirm-message'), self.data('confirm-button'), 'warning', function() {

            var fd = new FormData();
            var file = document.getElementById('license_image').files[0];

            if (file !== undefined) {
                fd.append('license_image', file);
            }

            $.each(self.find('input, select').not('[type=file],[type=submit]'), function(key, element) {
                element = $(element);
                console.log(element.attr('name'), element.val());
                fd.append(element.attr('name'), element.val());
            });

            $.ajax({
                url: self.attr('action'),
                data: fd,
                processData: false,
                contentType: false,
                encrypt: 'multipart/form-data',
                type: 'POST',
                success: function(response) {
                    notify(response);
                    checkErrors(response, function() {
                        modal.modal('hide');
                        pagination.refresh('license-listing');
                    });
                }
            });

        });

    });

    body.on('submit', 'form#update-license', function(e) {

        e.preventDefault();

        var self = $(this);

        confirm(self.data('confirm-message'), self.data('confirm-button'), 'warning', function() {

            var fd = new FormData();
            var file = document.getElementById('license_image').files[0];

            if (file !== undefined) {
                fd.append('license_image', file);
            }

            $.each(self.find('input, select').not('[type=file],[type=submit]'), function(key, element) {
                element = $(element);
                console.log(element.attr('name'), element.val());
                fd.append(element.attr('name'), element.val());
            });

            $.ajax({
                url: self.attr('action'),
                data: fd,
                processData: false,
                contentType: false,
                encrypt: 'multipart/form-data',
                type: 'POST',
                success: function(response) {
                    checkErrors(response, function() {
                        modal.modal('hide');
                        pagination.refresh('license-listing');
                        notify(response);
                    });
                }
            });

        });

    });

})(jQuery);