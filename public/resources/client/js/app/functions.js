function confirm(message, button, type, successCallback) {

    var confirmColor = '', cancelColor = '#818a91';
    switch(type) {
        case 'success':
            confirmColor = '#5cb85c';
            break;
        case 'warning':
            confirmColor = '#f0ad4e';
            break;
        case 'info':
            confirmColor = '#1213a1';
            break;
    }

    swal({
        title: message,
        type: type,
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonColor: confirmColor,
        cancelButtonColor: cancelColor,
        confirmButtonText: button
    }, successCallback);
}

function notify(response) {

    if (typeof response.responseJSON !== 'undefined') {
        response = response.responseJSON;
    }

    var type = 'success',
        title = 'Success!',
        returnString = '';

    if (typeof response.success === 'undefined') {

        type = 'error';
        title = 'Oops!';
        $.each(response, function(field) {
            returnString += '<p><span class="tag tag-danger">' + response[field] + '</span></p>';
        });

    }

    swal({
        title: title,
        text: returnString,
        html: true,
        type: type
    })

}

function checkErrors(response, onSuccess, onFailure) {

    if (typeof response.responseJSON !== 'undefined') {
        response = response.responseJSON;
    }

    if (typeof response.success === 'undefined') {

        var type = 'error',
            returnString = '',
            title = 'Oops!';

        $.each(response, function(field) {
            returnString += '<p><span class="tag tag-danger">' + response[field] + '</span></p>';
        });

        swal({
            title: title,
            text: returnString,
            html: true,
            type: type
        })

        if (typeof onFailure == 'function') {
            onFailure();
        }

    } else {

        if (typeof onSuccess == 'function') {
            onSuccess();
        }

    }

}