/**
 * @author Charles Moss
 */
(function($, undefined) {

    var body = $('body');

    var submitContent = false;
    body.on('submit', 'form#edit-content', function(e) {

        if (submitContent) {
            submitContent = false;
        } else {
            e.preventDefault();
        }

        var self = $(this);
        confirm(self.data('confirm-message'), self.data('confirm-button'), 'warning', function() {
            submitContent = true;
            self.submit();
        });

    });

    tinymce.init({ selector:'form#edit-content textarea' });

})(jQuery);