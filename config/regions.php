<?php

return [
    'us-states' => [
        'AK'=>'Alaska', 'HI'=>'Hawaii', 'CA'=>'California',
        'NV'=>'Nevada', 'OR'=>'Oregon', 'WA'=>'Washington',
        'AZ'=>'Arizona', 'CO'=>'Colorada', 'ID'=>'Idaho',
        'MT'=>'Montana', 'NE'=>'Nebraska', 'NM'=>'New Mexico',
        'ND'=>'North Dakota', 'UT'=>'Utah', 'WY'=>'Wyoming',
        'AL'=>'Alabama', 'AR'=>'Arkansas', 'IL'=>'Illinois',
        'IA'=>'Iowa', 'KS'=>'Kansas', 'KY'=>'Kentucky',
        'LA'=>'Louisiana', 'MN'=>'Minnesota', 'MS'=>'Mississippi',
        'MO'=>'Missouri', 'OK'=>'Oklahoma', 'SD'=>'South Dakota',
        'TX'=>'Texas', 'TN'=>'Tennessee', 'WI'=>'Wisconsin',
        'CT'=>'Connecticut', 'DE'=>'Delaware', 'FL'=>'Florida',
        'GA'=>'Georgia', 'IN'=>'Indiana', 'ME'=>'Maine', 'MD'=>'Maryland',
        'MA'=>'Massachusetts', 'MI'=>'Michigan', 'NH'=>'New Hampshire',
        'NJ'=>'New Jersey', 'NY'=>'New York', 'NC'=>'North Carolina',
        'OH'=>'Ohio', 'PA'=>'Pennsylvania', 'RI'=>'Rhode Island',
        'SC'=>'South Carolina', 'VT'=>'Vermont', 'VA'=>'Virginia',
        'WV'=>'West Virginia']
];