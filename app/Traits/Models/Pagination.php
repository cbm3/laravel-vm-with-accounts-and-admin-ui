<?php

namespace App\Traits\Models;

use Illuminate\Database\Eloquent\Builder;

trait Pagination {

    protected $paginationPerPage = 20;

    /**
     * @param Builder $query
     * @param int $page
     * @return Builder
     */
    public function scopePage(Builder $query, $page = 1)
    {
        $query->limit($this->paginationPerPage);
        if ($page > 1) {
            $query->offset($this->paginationPerPage * ($page - 1));
        }
        return $query;
    }

    /**
     * @param Builder $query
     * @param int $perPage
     * @return Builder
     */
    public function scopePerPage(Builder $query, int $perPage)
    {
        $this->paginationPerPage = $perPage;
        return $query;
    }

}