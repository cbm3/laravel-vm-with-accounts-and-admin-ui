<?php

namespace App\Mail;

use App\License;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LicenseExpiredEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var License
     */
    private $license;

    /**
     * @var User $user
     */
    private $user;

    /**
     * @var string $message
     */
    private $message;


    /**
     * ContactUsEmail constructor.
     * @param License $license
     * @param User    $user
     * @param string $message
     */
    public function __construct(License $license, User $user, string $message)
    {
        $this->license = $license;
        $this->user    = $user;
        $this->message = $message;
    }

    /**
     * @return $this
     */
    public function build()
    {
        $states = config('regions.us-states');
        $this->message = str_replace('{USER_NAME}', htmlentities($this->user->name), $this->message);
        $this->message = str_replace('{USER_EMAIL}', htmlentities($this->user->email), $this->message);
        $this->message = str_replace('{LICENSE_EXPIRATION_DATE}', $this->license->expires_at->format('m/d/Y'), $this->message);
        $this->message = str_replace('{LICENSE_STATE}', $states[$this->license->state], $this->message);

        return $this
            ->to($this->user->email)
            ->subject(_('Your license has expired!'))
            ->view('client.mail.license-expired-email')->with('data', ['message' => $this->message]);
    }
}