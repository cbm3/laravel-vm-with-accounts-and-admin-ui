<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUsEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var string
     */
    private $message;

    /**
     * ContactUsEmail constructor.
     * @param string $name
     * @param string $email
     * @param string $phoneNumber
     * @param string $message
     */
    public function __construct(string $name, string $email, string $phoneNumber, string $message)
    {
        $this->name         = $name;
        $this->email        = $email;
        $this->phoneNumber  = $phoneNumber;
        $this->message      = $message;
    }

    /**
     * @return $this
     */
    public function build()
    {
        $data = [
            'name' => $this->name,
            'email' => $this->email,
            'phoneNumber' => $this->phoneNumber,
            'message' => $this->message
        ];

        return $this
            ->to(config('mail.from')['address'])
            ->subject(_('New Entry For Contact Us!'))
            ->view('client.mail.contact-us-email')->with('data', $data);
    }
}