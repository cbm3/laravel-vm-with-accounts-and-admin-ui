<?php

namespace App\Http\Controllers;

use App\Content;
use App\License;
use App\Mail\ContactUsEmail;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\View\View;
use Illuminate\Mail\Mailer;
use Illuminate\Validation\Factory as Validator;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    private $auth;
    private $mailer;

    /**
     * ClientController constructor.
     *
     * @param AuthManager $auth
     * @param Mailer $mailer
     */
    public function __construct(AuthManager $auth, Mailer $mailer)
    {
        $this->auth = $auth;
        $this->mailer = $mailer;
    }

    /**
     * @return View
     */
    public function index()
    {
        return view('client.home');
    }

    /**
     * @return View
     */
    public function privacyPolicy()
    {
        $content = Content::where('identifier', '=', 'PRIVACY_POLICY')->firstOrFail();
        return view('client.privacy-policy')->with(['content' => $content]);
    }

    /**
     * @return View
     */
    public function termsOfUse()
    {
        $content = Content::where('identifier', '=', 'TERMS_OF_USE')->firstOrFail();
        return view('client.terms-of-use')->with(['content' => $content]);
    }

    /**
     * @return View
     */
    public function aboutUs()
    {
        $content = Content::where('identifier', '=', 'ABOUT_US')->firstOrFail();
        return view('client.about-us')->with(['content' => $content]);
    }

    /**
     * @return View
     */
    public function contactUs()
    {
        return view('client.contact-us')->with(['authUser' => $this->auth->user()]);
    }

    /**
     * @param Request $request
     * @param Validator $validator
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendContactEmail(Request $request, Validator $validator)
    {
        $validation = $validator->make($request->all(), [
            'name'          => 'required|max:40',
            'email'         => 'required|email|max:60',
            'phone-number'  => 'required|max:20',
            'message'       => 'required|max:500'
        ]);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        $response = new ContactUsEmail(
            $request->input('name'),
            $request->input('email'),
            $request->input('phone-number'),
            $request->input('message')
        );

        $this->mailer->queue($response);

        return redirect()->back()->with('success', _('Thank you for your input, your message has been sent!'));
    }

    /**
     * @return View
     */
    public function licenseLocations()
    {
        $states = License::notExpired()->availableStates()->get();
        return view('client.licenses-map')->with('states', $states);
    }
}
