<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Content;
use Illuminate\Http\Request;
use Illuminate\Validation\Factory as Validator;

class ContentController extends Controller
{
    public function edit(string $uri)
    {
        switch($uri) {
            case 'about-us':
                $identifier = 'ABOUT_US';
                break;
            case 'terms-of-use':
                $identifier = 'TERMS_OF_USE';
                break;
            case 'privacy-policy':
                $identifier = 'PRIVACY_POLICY';
                break;
            case 'license-expiration-email':
                $identifier = 'LICENSE_EXPIRATION_EMAIL';
                break;
            default:
                abort(404); exit;
                break;
        }
        $content = Content::where('identifier', '=', $identifier)->firstOrFail();
        return view('admin.content.single')->with(['content' => $content, 'uri' => $uri]);
    }

    public function update(Validator $validator, Request $request, $identifier)
    {
        $content = Content::where('identifier', '=', $identifier)->firstOrFail();
        $validation = $validator->make($request->all(), ['content' => 'required|max:65000']);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        $content->content = $request->input('content');
        $content->save();

        return redirect()->back()->with('success', _('Content was updated successfully!'));
    }
}
