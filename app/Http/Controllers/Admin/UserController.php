<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Factory as Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index()
    {
        return view('admin.user.list');
    }

    public function list(Request $request)
    {
        $page    = (int) $request->input('page', 1);
        $perPage = (int) $request->input('per-page', 20);
        $search  = (string) $request->input('search', '');
        $users   = User::perPage($perPage)->page($page);

        if (!empty($search)) {
            $users->search($search);
        }

        return view('admin.user.partial.loop')->with('users', $users->get());
    }

    public function edit(int $userId)
    {
        $user = User::findOrFail($userId);
        return view('admin.user.single')->with('user', $user);
    }

    public function update(Validator $validator, Request $request, int $userId)
    {
        $user = User::findOrFail($userId);
        $validation = $validator->make($request->all(), [
            'name'              => 'required|max:60',
            'email'             => 'required|email|unique:users,email,'.$user->id,
            'password'          => 'confirmed',
            'admin'             => 'integer|min:1|max:1',
            'activated'         => 'integer|min:1|max:1',
            'address_line_1'    => 'required|string|min:5|max:40',
            'address_line_2'    => 'max:40',
            'city'              => 'required|string|min:4|max:40',
            'state'             => ['required', Rule::in(array_keys(config('regions.us-states')))],
            'zip'               => 'required|integer|digits_between:4,8',
            'phone_number'      => 'required|max:40',
            'birthday'          => ['required', 'regex:/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/'],
            'company_name'      => 'required|string|max:40',
            'job_title'         => 'required|string|max:40'
        ]);

        if ($request->input('admin')) {
            $user->admin = 1;
        } else {
            $user->admin = 0;
        }

        if ($request->input('activated')) {
            $user->deactivated = 0;
        } else {
            $user->deactivated = 1;
        }

        if (!empty($request->input('password'))) {
            $user->password = bcrypt($request->input('password'));
        }

        $user->email = $request->input('email');
        $user->name = $request->input('name');

        $contact = $user->contact;
        $contact->address_line_1 = $request->input('address_line_1');
        $contact->address_line_2 = $request->input('address_line_2');
        $contact->city           = $request->input('city');
        $contact->state          = $request->input('state');
        $contact->zip            = $request->input('zip');
        $contact->phone_number   = $request->input('phone_number');
        $contact->birthday       = strtotime($request->input('birthday'));
        $contact->company_name   = $request->input('company_name');
        $contact->job_title      = $request->input('job_title');

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        $user->save();
        $contact->save();

        return redirect()->back()->with('success', _('User was updated successfully!'));
    }

    /**
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function activate(int $userId)
    {
        $user = User::findOrFail($userId);
        $user->deactivated = 0;
        $user->save();
        return response()->json(['success' => 1]);
    }

    /**
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function deactivate(int $userId)
    {
        $user = User::findOrFail($userId);
        $user->deactivated = 1;
        $user->save();
        return response()->json(['success' => 1]);
    }
}
