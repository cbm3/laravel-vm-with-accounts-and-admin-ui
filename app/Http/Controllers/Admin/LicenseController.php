<?php

namespace App\Http\Controllers\Admin;

use App\License;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Validation\Factory as Validator;
use Illuminate\Validation\Rule;

class LicenseController extends Controller
{
    /**
     * @param int $userId
     * @return View
     */
    public function new(int $userId)
    {
        $user = User::findOrFail($userId);
        return view('admin.license.partial.new')->with('user', $user);
    }

    /**
     * @param Validator $validator
     * @param Request $request
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Validator $validator, Request $request, int $userId)
    {
        $validation = $validator->make($request->all(), [
            'state' => [
                'required', 'min:2','max:2',
                Rule::unique('licenses')->where(function ($query) use ($userId) {
                    $query->where('user_id', $userId);
                })
            ],
            'title' => 'required|max:40',
            'number' => 'required|max:40|unique:licenses,number',
            'trade' => 'required|max:40|in:AKadmin,AKjourn,Alarm,Apprentice,Automotive,Contractor,Electrical,Engineer,HVACR,Inspector,NonCourse,Security',
            'license_image' => 'image|mimes:jpeg,png',
            'licensure_date' => ['required', 'regex:/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/'],
            'last_renewal_date' => ['required', 'regex:/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/'],
            'expires_at' => ['required', 'regex:/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/']
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors());
        }

        if ($request->hasFile('license_image') && $request->file('license_image')->isValid()) {
            $uploadedFile = $request->file('license_image');
            $licenseImgUid = uniqid('license_image_').'.'.$uploadedFile->getClientOriginalExtension();
            $uploadedFile->move(public_path().'/uploads', $licenseImgUid);
        } else {
            $licenseImgUid = null;
        }

        $license = new License;
        $license->user_id = $userId;
        $license->image_uid = $licenseImgUid;
        $license->title = $request->input('title');
        $license->number = $request->input('number');
        $license->trade = $request->input('trade');
        $license->licensure_date = strtotime($request->input('licensure_date'));
        $license->last_renewal_date = strtotime($request->input('last_renewal_date'));
        $license->expires_at = strtotime($request->input('expires_at'));
        $license->state = $request->input('state');
        $license->save();

        return response()->json(['success' => 1]);
    }

    /**
     * @param int $licenseId
     * @return View
     */
    public function edit(int $licenseId)
    {
        $license = License::findOrFail($licenseId);
        return view('admin.license.partial.edit')->with('license', $license);
    }

    /**
     * @param Validator $validator
     * @param Request $request
     * @param int $licenseId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Validator $validator, Request $request, int $licenseId)
    {
        $license = License::findOrFail($licenseId);
        $validation = $validator->make($request->all(), [
            'state' => [
                'required', 'min:2','max:2',
                Rule::unique('licenses')->ignore($license->id)->where(function ($query) use ($license) {
                    $query->where('user_id', $license->user_id);
                })
            ],
            'title' => 'required|max:40',
            'number' => 'required|max:40|unique:licenses,number,'.$license->id,
            'trade' => 'required|max:40|in:AKadmin,AKjourn,Alarm,Apprentice,Automotive,Contractor,Electrical,Engineer,HVACR,Inspector,NonCourse,Security',
            'license_image' => 'image|mimes:jpeg,png',
            'licensure_date' => ['required', 'regex:/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/'],
            'last_renewal_date' => ['required', 'regex:/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/'],
            'expires_at' => ['required', 'regex:/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/']
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors());
        }

        if ($request->hasFile('license_image') && $request->file('license_image')->isValid()) {

            $uploadedFile = $request->file('license_image');

            if ($license->image_uid === null) {
                $license->image_uid = uniqid('license_image_').'.'.$uploadedFile->getClientOriginalExtension();
            }

            $uploadedFile->move(public_path().'/uploads', $license->image_uid);

        }

        $license->title = $request->input('title');
        $license->number = $request->input('number');
        $license->trade = $request->input('trade');
        $license->licensure_date = strtotime($request->input('licensure_date'));
        $license->last_renewal_date = strtotime($request->input('last_renewal_date'));
        $license->expires_at = strtotime($request->input('expires_at'));
        $license->state = $request->input('state');
        $license->save();

        return response()->json(['success' => 1]);
    }

    /**
     * @param int $licenseId
     * @return View
     */
    public function reviewImage(int $licenseId)
    {
        $license = License::findOrFail($licenseId);
        return view('admin.license.partial.review-image')->with('license', $license);
    }

    /**
     * @param int $licenseId
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteImage(int $licenseId)
    {
        $license = License::findOrFail($licenseId);
        if (file_exists(public_path().'/uploads/'.$license->image_uid)) {
            @unlink(public_path().'/uploads/'.$license->image_uid);
        }
        $license->image_uid = null;
        $license->save();
        return response()->json(['success' => 1]);
    }

    /**
     * @param int $licenseId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $licenseId)
    {
        $license = License::findOrFail($licenseId);
        $license->delete();
        return response()->json(['success' => 1]);
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return View
     */
    public function list(Request $request, int $userId)
    {
        $page       = (int) $request->input('page', 1);
        $perPage    = (int) $request->input('per-page', 20);
        $state      = (string) $request->input('state', '');
        $licenses   = License::perPage($perPage)->page($page)->user($userId);

        if (!empty($state)) {
            $licenses->state($state);
        }

        return view('admin.license.partial.loop')->with('licenses', $licenses->get());
    }
}
