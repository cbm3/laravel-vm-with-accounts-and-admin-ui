<?php

namespace App\Http\Controllers;

use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Validation\Factory as Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    private $auth;
    private $request;
    private $validator;

    public function __construct(AuthManager $auth,  Request $request, Validator $validator)
    {
        $this->auth         = $auth;
        $this->validator    = $validator;
        $this->request      = $request;
    }

    /**
     * @param AuthManager $auth
     * @return View
     */
    public function account(AuthManager $auth)
    {
        return view('client.account')->with('user', $auth->user());
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update()
    {
        $user = $this->auth->user();
        $validation = $this->validator->make($this->request->all(), [
            'name'      => 'required|max:60',
            'email'     => 'required|email|unique:users,email,'.$user->id,
            'password'  => 'confirmed',
            'address_line_1'    => 'required|string|min:5|max:40',
            'address_line_2'    => 'max:40',
            'city'              => 'required|string|min:4|max:40',
            'state'             => ['required', Rule::in(array_keys(config('regions.us-states')))],
            'zip'               => 'required|integer|digits_between:4,8',
            'phone_number'      => 'required|max:40',
            'birthday'          => ['required', 'regex:/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/'],
            'company_name'      => 'required|string|max:40',
            'job_title'         => 'required|string|max:40'
        ]);

        if (!empty($this->request->input('password'))) {
            $user->password = bcrypt($this->request->input('password'));
        }

        $user->email = $this->request->input('email');
        $user->name = $this->request->input('name');

        $contact = $user->contact;
        $contact->address_line_1 = $this->request->input('address_line_1');
        $contact->address_line_2 = $this->request->input('address_line_2');
        $contact->city           = $this->request->input('city');
        $contact->state          = $this->request->input('state');
        $contact->zip            = $this->request->input('zip');
        $contact->phone_number   = $this->request->input('phone_number');
        $contact->birthday       = strtotime($this->request->input('birthday'));
        $contact->company_name   = $this->request->input('company_name');
        $contact->job_title      = $this->request->input('job_title');

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        $user->save();
        $contact->save();

        return redirect()->back()->with('success', _('Your account was updated successfully!'));
    }
}
