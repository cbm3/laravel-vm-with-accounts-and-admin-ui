<?php

namespace App\Http\Controllers\Auth;

use App\Contact;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\Rule;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/account';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'              => 'required|string|max:255',
            'email'             => 'required|string|email|max:255|unique:users',
            'password'          => 'required|string|min:6|confirmed',
            'address_line_1'    => 'required|string|min:5|max:40',
            'address_line_2'    => 'max:40',
            'city'              => 'required|string|min:4|max:40',
            'state'             => ['required', Rule::in(array_keys(config('regions.us-states')))],
            'zip'               => 'required|integer|digits_between:4,8',
            'phone_number'      => 'required|max:40',
            'birthday'          => ['required', 'regex:/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/'],
            'company_name'      => 'required|string|max:40',
            'job_title'         => 'required|string|max:40'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user =  User::create([
            'name'              => $data['name'],
            'email'             => $data['email'],
            'password'          => bcrypt($data['password']),
        ]);

        Contact::create([
            'user_id'           => $user->id,
            'address_line_1'    => $data['address_line_1'],
            'address_line_2'    => $data['address_line_2'],
            'city'              => $data['city'],
            'state'             => $data['state'],
            'zip'               => $data['zip'],
            'phone_number'      => $data['phone_number'],
            'birthday'          => strtotime($data['birthday']),
            'company_name'      => $data['company_name'],
            'job_title'         => $data['job_title']
        ]);

        return $user;
    }
}
