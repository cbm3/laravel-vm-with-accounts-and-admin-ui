<?php

namespace App\Console\Commands;

use App\Content;
use App\License;
use App\Mail\LicenseExpiredEmail;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;

class SendExpiredLicensesNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:expired-licenses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify users of their expired licenses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute console command
     *
     * @param Mailer $mailer
     * @param Request $request
     */
    public function handle(Mailer $mailer, Request $request)
    {
        License::expired()->notNotifiedOfExpiration()->chunk(300, function($licenses) use ($mailer, $request) {

            foreach ($licenses as $license) {

                $email = new LicenseExpiredEmail(
                    $license,
                    $license->user,
                    Content::where('identifier', '=', 'LICENSE_EXPIRATION_EMAIL')->firstOrFail()->content
                );

                $this->info("Sending license expiration email to " . $license->user->email . '...');
                $mailer->queue($email);

                $license->notified_of_expiration = 1;
                $license->save();

            }

        });
    }
}
