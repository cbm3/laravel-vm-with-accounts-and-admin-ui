<?php

namespace App;

use App\Traits\Models\Pagination;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class License extends Model
{
    use Pagination;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'expires_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The columns that are used to store date values
     *
     * @var array
     */
    protected $dates = [
        'updated_at',
        'created_at',
        'expires_at',
        'licensure_date',
        'last_renewal_date'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @param Builder $query
     * @param int $userId
     * @return Builder
     */
    public function scopeUser(Builder $query, int $userId)
    {
        return $query->where('user_id', '=', $userId);
    }

    /**
     * @param Builder $query
     * @param string $state
     * @return Builder
     */
    public function scopeState(Builder $query, string $state)
    {
        return $query->where('state', '=', $state);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeExpired(Builder $query)
    {
        return $query->whereRaw('UNIX_TIMESTAMP(expires_at) <= '.time());
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeNotExpired(Builder $query)
    {
        return $query->whereRaw('UNIX_TIMESTAMP(expires_at) > '.time());
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeNotNotifiedOfExpiration(Builder $query)
    {
        return $query->where('notified_of_expiration' ,'=', 0);
    }

    /**
     * @param Builder $query
     */
    public function scopeAvailableStates(Builder $query)
    {
        $query->select(DB::raw('count(licenses.id) as count, licenses.state as abbreviation'))
              ->join('users', 'users.id', '=', 'licenses.user_id')
              ->where('users.deactivated', '=', '0')
              ->groupBy('licenses.state');
    }
}
