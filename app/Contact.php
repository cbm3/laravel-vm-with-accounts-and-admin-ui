<?php

namespace App;

use \Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'address_line_1', 'address_line_2',
        'city', 'state', 'zip', 'phone_number',
        'birthday', 'company_name', 'job_title'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The columns that are used to store date values
     *
     * @var array
     */
    protected $dates = [
        'updated_at',
        'created_at',
        'birthday'
    ];
}
