@extends('admin.template')

@section('page-title', config('app.name'))

@section('page-css')
    <link rel="stylesheet" type="text/css" href="{{ url('resources/bower/intl-tel-input/build/css/intlTelInput.css') }}"/>
@endsection

@section('page-js')
    <script src="{{ url('resources/bower/intl-tel-input/build/js/intlTelInput.js') }}"></script>
    <script src="{{ url('resources/admin/js/user.js') }}"></script>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-group"></i> {{ _('Users') }}
                <small>{{ _('Manage user accounts') }}</small>
            </h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> {{ _('Admin') }}</li>
                <li><i class="fa fa-group"></i> {{ _('Users') }}</li>
                <li class="active"><i class="fa fa-user"></i> {{ $user->name }}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!--Main Row-->
            <div class="row">

                <!--Main Column-->
                <div class="col-xs-12">

                    <h3>{{ _('User\'s Information') }}</h3>

                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('success') }}
                        </div>
                    @endif

                    <form action="{{ url('admin/user/'.$user->id.'/update') }}"
                          method="POST"
                          id="edit-user"
                          data-confirm-button="{{ _('Yes, I\'m Sure') }}"
                          data-confirm-message="{{ _('Are you sure you\'re ready to submit') }}">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="row">

                            <div class="col-xs-12">

                                <div class="row">

                                    <div class="col-xs-12 col-md-6 col-md-offset-6">

                                        <div class="row">

                                            <div class="col-xs-12 col-md-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon">{{ _('Activated') }}</span>
                                                    <input type="checkbox"
                                                           name="activated"
                                                           class="fancy-checkbox"
                                                           {{ old('activated', !$user->deactivated) ? 'checked' : '' }} value="1">
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-md-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon">{{ _('Admin') }}</span>
                                                    <input type="checkbox"
                                                           name="admin"
                                                           class="fancy-checkbox"
                                                           {{ old('admin', $user->admin) ? 'checked' : '' }} value="1">
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <br/>

                                <div class="row">

                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label for="name" class="col-md-4 control-label">Name</label>

                                            <div class="col-md-6">
                                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name', $user->name) }}" required>
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="address-line-1" class="col-md-4 control-label">Email</label>

                                            <div class="col-md-6">
                                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email', $user->email) }}" required>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <br/>

                                <div class="row">

                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="col-md-4 control-label">Password</label>

                                            <div class="col-md-6">
                                                <input id="password" type="text" class="form-control" name="password" value="{{ old('password') }}">
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                            <label for="password" class="col-md-4 control-label">Confirm Password</label>

                                            <div class="col-md-6">
                                                <input id="password" type="text" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}">
                                                @if ($errors->has('password_confirmation'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <br/>

                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group{{ $errors->has('address_line_1') ? ' has-error' : '' }}">
                                            <label for="address-line-1" class="col-md-4 control-label">Address</label>

                                            <div class="col-md-6">
                                                <input id="address-line-1" type="text" class="form-control" name="address_line_1" value="{{ old('address_line_1', $user->contact->address_line_1) }}" required>
                                                @if ($errors->has('address_line_1'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('address_line_1') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group{{ $errors->has('address_line_2') ? ' has-error' : '' }}">
                                            <label for="address-line-2" class="col-md-4 control-label">Address Line 2</label>

                                            <div class="col-md-6">
                                                <input id="address-line-2" type="text" class="form-control" name="address_line_2" value="{{ old('address_line_2', $user->contact->address_line_2) }}">
                                                @if ($errors->has('address_line_2'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('address_line_2') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br/>

                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                            <label for="city" class="col-md-4 control-label">City</label>

                                            <div class="col-md-6">
                                                <input id="city" type="text" class="form-control" name="city" value="{{ old('city', $user->contact->city) }}">
                                                @if ($errors->has('city'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                            <label for="state" class="col-md-4 control-label">State</label>

                                            <div class="col-md-6">
                                                <select name="state" class="form-control">
                                                    @foreach(config('regions.us-states') as $abbreviation => $name)
                                                        <option value="{{ $abbreviation }}"{{old('state', $user->contact->state) == $abbreviation ? ' selected' : ''}}>{{ $name }} ({{ $abbreviation }})</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('state'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br/>

                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
                                            <label for="zip" class="col-md-4 control-label">Zip Code</label>

                                            <div class="col-md-6">
                                                <input id="zip" type="text" class="form-control" name="zip" value="{{ old('zip', $user->contact->zip) }}">
                                                @if ($errors->has('zip'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('zip') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                            <label for="phone_number" class="col-md-4 control-label">Phone Number</label>

                                            <div class="col-md-6">
                                                <input id="phone_number" type="text" class="form-control phone-number-input" name="phone_number" value="{{ old('phone_number', $user->contact->phone_number) }}">
                                                @if ($errors->has('phone_number'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('phone_number') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br/>

                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                                            <label for="birthday" class="col-md-4 control-label">Birthday</label>

                                            <div class="col-md-6">
                                                <input id="birthday" type="text" class="form-control date-picker" name="birthday" value="{{ old('birthday', $user->contact->birthday->format('m/d/Y')) }}">
                                                @if ($errors->has('birthday'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('birthday') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                                            <label for="phone_number" class="col-md-4 control-label">Company Name</label>

                                            <div class="col-md-6">
                                                <input id="company_name" type="text" class="form-control" name="company_name" value="{{ old('company_name', $user->contact->company_name) }}">
                                                @if ($errors->has('company_name'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('company_name') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br/>

                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group{{ $errors->has('job_title') ? ' has-error' : '' }}">
                                            <label for="job_title" class="col-md-4 control-label">Job Title</label>

                                            <div class="col-md-6">
                                                <input id="job_title" type="text" class="form-control" name="job_title" value="{{ old('job_title', $user->contact->job_title) }}">
                                                @if ($errors->has('job_title'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('job_title') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <input type="submit" class="btn btn-flat btn-success btn-block" value="{{ _('Save Changes') }}">
                                    </div>
                                </div>

                            </div>

                        </div>

                    </form>

                    <hr/>

                    <div class="pagination-container"
                         data-alias="license-listing"
                         data-address="{{ url('admin/license/list/'.$user->id) }}"
                         data-page="1"
                         data-per-page="8">

                        <form class="filter-inputs">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <h3>{{ _('User\'s Licenses') }}</h3>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                    <br/>
                                    <div class="input-group">
                                        <select name="state" class="form-control">
                                            <option value="">{{_('Select a state')}}</option>
                                            @foreach(config('regions.us-states') as $abbreviation => $name)
                                                <option value="{{ $abbreviation }}">{{ $name }}({{ $abbreviation }})</option>
                                                @endforeach
                                        </select>
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-2">
                                    <br/>
                                    <a href="{{ url('admin/license/new/'.$user->id) }}"
                                       class="btn btn-flat btn-sm btn-success btn-block add-license"
                                       data-toggle="tooltip"
                                       data-original-title="{{ _('Add License') }}"><i class="fa fa-plus"></i> {{ _('Add License') }}</a>
                                </div>
                            </div>
                        </form>
                        <br/>
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <td width="12%" class="text-center">{{ _('Title') }}</td>
                                <td width="12%" class="text-center">{{ _('Number') }}</td>
                                <td width="12%" class="text-center">{{ _('State') }}</td>
                                <td width="12%" class="text-center">{{ _('Licensure Date') }}</td>
                                <td width="12%" class="text-center">{{ _('Last Renewal') }}</td>
                                <td width="12%" class="text-center">{{ _('Expiration') }}</td>
                                <td width="12%" class="text-center">{{ _('Expired') }}</td>
                                <td width="12%" class="text-center">{{ _('Action') }}</td>
                            </tr>
                            </thead>
                            <tbody class="dynamic-html"></tbody>
                        </table>
                        <div class="loading-indicator hide">
                            <h1 class="text-center" style="height: 300px;line-height:300px;">
                                <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                            </h1>
                        </div>

                        <div class="pull-right">
                            <span class="btn btn-default last-page"><i class="fa fa-arrow-left"></i></span>
                            <span class="btn btn-default next-page"><i class="fa fa-arrow-right"></i></span>
                        </div>

                    </div>

                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>
@endsection