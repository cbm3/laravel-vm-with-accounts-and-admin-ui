@extends('admin.template')

@section('page-title', config('app.name'))

@section('page-js')
    <script src="{{ url('resources/admin/js/user.js') }}"></script>
    @endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-group"></i> {{ _('Users') }}
                <small>{{ _('Manage user accounts') }}</small>
            </h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> {{ _('Admin') }}</li>
                <li class="active"><i class="fa fa-group"></i> {{ _('Users') }}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!--Main Row-->
            <div class="row">

                <!--Main Column-->
                <div class="col-xs-12">

                    <div class="pagination-container"
                         data-alias="users-listing"
                         data-address="{{ url('admin/user/list') }}"
                         data-page="1"
                         data-per-page="15">

                        <form class="filter-inputs">
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-md-offset-6">
                                    <div class="input-group">
                                        <input name="search"
                                               type="text"
                                               class="form-control"
                                               placeholder="{{ _('Search users by email or name') }}">
                                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <br/>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <td width="20%" class="text-center">{{ _('Name') }}</td>
                                    <td width="20%" class="text-center">{{ _('Email') }}</td>
                                    <td width="20%" class="text-center">{{ _('Admin') }}</td>
                                    <td width="20%" class="text-center">{{ _('Member Since') }}</td>
                                    <td width="20%" class="text-center">{{ _('Action') }}</td>
                                </tr>
                            </thead>
                            <tbody class="dynamic-html"></tbody>
                        </table>
                        <div class="loading-indicator hide">
                            <h1 class="text-center" style="height: 500px;line-height:500px;">
                                <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                            </h1>
                        </div>

                        <div class="pull-right">
                            <span class="btn btn-default last-page"><i class="fa fa-arrow-left"></i></span>
                            <span class="btn btn-default next-page"><i class="fa fa-arrow-right"></i></span>
                        </div>

                    </div>

                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>
    @endsection