@if(empty($users))
    {{ _('No results found') }}
@else
    @foreach($users as $user)
        <tr>
            <td class="text-center">{{ $user->name }}</td>
            <td class="text-center">{{ $user->email }}</td>
            <td class="text-center">{!! $user->admin == 1 ? "<i class='fa fa-check'></i>" : "<i class='fa fa-times'></i>" !!}</td>
            <td class="text-center">{{ $user->created_at->format('m/d/Y') }}</td>
            <td class="text-center">
                <a href="{{ url('admin/user/' . $user->id) }}"
                   class="btn btn-sm btn-info btn-flat"
                   data-toggle="tooltip"
                   data-original-title="{{ _('Review User') }}"><i class="fa fa-book"></i></a>
                @if ($user->deactivated)
                    <a href="{{ url('admin/user/activate/' . $user->id) }}"
                       class="btn btn-sm btn-success btn-flat activate-user"
                       data-confirm-button="{{ _('Yes, I\'m sure') }}"
                       data-confirm-message="{{ _('Are you sure you want to activate this user?') }}"
                       data-toggle="tooltip"
                       data-original-title="{{ _('Activate User') }}"><i class="fa fa-plus"></i></a>
                @else
                    <a href="{{ url('admin/user/deactivate/' . $user->id) }}"
                       class="btn btn-sm btn-warning btn-flat deactivate-user"
                       data-confirm-button="{{ _('Yes, I\'m sure') }}"
                       data-confirm-message="{{ _('Are you sure you want to deactivate this user?') }}"
                       data-toggle="tooltip"
                       data-original-title="{{ _('Deactivate User') }}"><i class="fa fa-minus"></i></a>
                @endif
            </td>
        </tr>
        @endforeach
@endif