<div class="modal-body">

    <h4 class="m-b-20">{{ _("Add License") }}</h4>
    <hr/>

    <div class="row">

        <div class="col-xs-12">

            <form action="{{ url('admin/license/create/'.$user->id) }}"
                  id="add-license"
                  data-confirm-message="{{ _('Are you sure you\'re ready to submit?') }}"
                  data-confirm-button="{{ _('Yes I\'m Sure') }}">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <label for="title">{{ _('Title') }}</label>
                        <div class="input-group">
                            <input type="text"
                                   id="title"
                                   class="form-control"
                                   name="title"
                                   placeholder="{{ _('Title') }}"
                                   value="">
                            <span class="input-group-addon"><i class="fa fa-book"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <label for="number">{{ _('Number / ID') }}</label>
                        <div class="input-group">
                            <input type="text"
                                   id="number"
                                   class="form-control"
                                   name="number"
                                   placeholder="{{ _('Number / ID') }}"
                                   value="">
                            <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                        </div>
                    </div>
                </div>

                <br/>

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <label for="licensure_date">{{ _('Licensure Date') }}</label>
                        <div class="input-group">
                            <input type="text"
                                   id="licensure_date"
                                   class="form-control date-picker"
                                   name="licensure_date"
                                   placeholder="{{ _('Licensure Date') }}"
                                   value="">
                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <label for="trade">{{ _('Trade') }}</label>
                        <div class="input-group">
                            <select class="form-control" name="trade" id="trade">
                                <option>{{_('Select a Trade') }}</option>
                                <option value="AKadmin">{{ _('AKadmin') }}</option>
                                <option value="AKjourn">{{ _('AKjourn') }}</option>
                                <option value="Alarm">{{ _('Alarm') }}</option>
                                <option value="Apprentice">{{ _('Apprentice') }}</option>
                                <option value="Automotive">{{ _('Automotive') }}</option>
                                <option value="Contractor">{{ _('Contractor') }}</option>
                                <option value="Electrical">{{ _('Electrical') }}</option>
                                <option value="Engineer">{{ _('Engineer') }}</option>
                                <option value="HVACR">{{ _('HVACR') }}</option>
                                <option value="Inspector">{{ _('Inspector') }}</option>
                                <option value="NonCourse">{{ _('NonCourse') }}</option>
                                <option value="Security">{{ _('Security') }}</option>
                            </select>
                            <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                        </div>
                    </div>
                </div>

                <br/>

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <label for="last_renewal_date">{{ _('Last Renewal Date') }}</label>
                        <div class="input-group">
                            <input type="text"
                                   class="form-control date-picker"
                                   name="last_renewal_date"
                                   placeholder="{{ _('Last Renewal Date') }}"
                                   value="">
                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <label for="state">{{ _('State') }}</label>
                        <div class="input-group">
                            <select class="form-control" name="state" id="state">
                                <option value="">{{ _('Select a State') }}</option>
                                @foreach(config('regions.us-states') as $abbreviation => $title)
                                    <option value="{{ $abbreviation }}">{{ $title}}({{ $abbreviation }})</option>
                                @endforeach
                            </select>
                            <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                        </div>
                    </div>
                </div>

                <br/>

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <label for="expires_at">{{ _('Expiration Date') }}</label>
                        <div class="input-group">
                            <input type="text"
                                   id="expires_at"
                                   class="form-control date-picker"
                                   name="expires_at"
                                   placeholder="{{ _('Expiration Date') }}"
                                   value="">
                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <label for="license_image">{{ _('License Image') }} ({{ _('optional') }})</label>
                        <div class="input-group">
                            <input type="file"
                                   id="license_image"
                                   class="form-control"
                                   name="license_image">
                            <span class="input-group-addon"><i class="fa fa-upload"></i></span>
                        </div>
                    </div>
                </div>

                <br/>

                <div class="row">
                    <div class="col-xs-12 col-md-4 col-md-offset-8">
                        <input type="submit" class="btn btn-flat btn-sm btn-success btn-block" value="{{ _('Create License') }}">
                    </div>
                </div>
            </form>

        </div>

    </div>
</div>