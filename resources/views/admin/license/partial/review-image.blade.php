<div class="modal-body">

    <h4 class="m-b-20">{{ _("License Image") }}</h4>
    <hr/>

    <img src="{{ url('uploads/'.$license->image_uid) }}?time={{time()}}" alt="{{ _('License Image') }}" style="width: 100%; height: auto;">

    <br/>
    <br/>

    <a href="{{ url('admin/license/'.$license->id.'/delete-image') }}"
       data-confirm-button="{{ _('Yes, I\'m Sure') }}"
       data-confirm-message="{{ _('Are you sure you want to delete this license image?') }}"
       class="btn btn-danger delete-image pull-right">{{ _('Delete Image') }}</a>

    <div style="clear: both;"></div>
</div>