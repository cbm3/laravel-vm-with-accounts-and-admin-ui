<div class="modal-body">

    <h4 class="m-b-20">{{ _("Update License") }}</h4>
    <hr/>

    <div class="row">

        <div class="col-xs-12">

            <form action="{{ url('admin/license/'.$license->id.'/update') }}"
                  id="update-license"
                  data-confirm-message="{{ _('Are you sure you\'re ready to submit?') }}"
                  data-confirm-button="{{ _('Yes I\'m Sure') }}">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <label for="title">{{ _('Title') }}</label>
                        <div class="input-group">
                            <input type="text"
                                   id="title"
                                   class="form-control"
                                   name="title"
                                   placeholder="{{ _('Title') }}"
                                   value="{{ $license->title }}">
                            <span class="input-group-addon"><i class="fa fa-book"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <label for="number">{{ _('Number / ID') }}</label>
                        <div class="input-group">
                            <input type="text"
                                   id="number"
                                   class="form-control"
                                   name="number"
                                   placeholder="{{ _('Number / ID') }}"
                                   value="{{ $license->number }}">
                            <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                        </div>
                    </div>
                </div>

                <br/>

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <label for="licensure_date">{{ _('Licensure Date') }}</label>
                        <div class="input-group">
                            <input type="text"
                                   id="licensure_date"
                                   class="form-control date-picker"
                                   name="licensure_date"
                                   placeholder="{{ _('Licensure Date') }}"
                                   value="{{ $license->licensure_date->format('m/d/Y') }}">
                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <label for="trade">{{ _('Trade') }}</label>
                        <div class="input-group">
                            <select class="form-control" name="trade" id="trade">
                                <option>{{_('Select a Trade') }}</option>
                                <option value="AKadmin"{{ $license->trade == 'AKadmin' ? ' selected' : '' }}>{{ _('AKadmin') }}</option>
                                <option value="AKjourn"{{ $license->trade == 'AKjourn' ? ' selected' : '' }}>{{ _('AKjourn') }}</option>
                                <option value="Alarm"{{ $license->trade == 'Alarm' ? ' selected' : '' }}>{{ _('Alarm') }}</option>
                                <option value="Apprentice"{{ $license->trade == 'Apprentice' ? ' selected' : '' }}>{{ _('Apprentice') }}</option>
                                <option value="Automotive"{{ $license->trade == 'Automotive' ? ' selected' : '' }}>{{ _('Automotive') }}</option>
                                <option value="Contractor"{{ $license->trade == 'Contractor' ? ' selected' : '' }}>{{ _('Contractor') }}</option>
                                <option value="Electrical"{{ $license->trade == 'Electrical' ? ' selected' : '' }}>{{ _('Electrical') }}</option>
                                <option value="Engineer"{{ $license->trade == 'Engineer' ? ' selected' : '' }}>{{ _('Engineer') }}</option>
                                <option value="HVACR"{{ $license->trade == 'HVACR' ? ' selected' : '' }}>{{ _('HVACR') }}</option>
                                <option value="Inspector"{{ $license->trade == 'Inspector' ? ' selected' : '' }}>{{ _('Inspector') }}</option>
                                <option value="NonCourse"{{ $license->trade == 'NonCourse' ? ' selected' : '' }}>{{ _('NonCourse') }}</option>
                                <option value="Security"{{ $license->trade == 'Security' ? ' selected' : '' }}>{{ _('Security') }}</option>
                            </select>
                            <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                        </div>
                    </div>
                </div>

                <br/>

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <label for="last_renewal_date">{{ _('Last Renewal Date') }}</label>
                        <div class="input-group">
                            <input type="text"
                                   class="form-control date-picker"
                                   name="last_renewal_date"
                                   placeholder="{{ _('Last Renewal Date') }}"
                                   value="{{ $license->last_renewal_date->format('m/d/Y') }}">
                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <label for="state">{{ _('State') }}</label>
                        <div class="input-group">
                            <select class="form-control" name="state" id="state">
                                <option value="">{{ _('Select a State') }}</option>
                                @foreach(config('regions.us-states') as $abbreviation => $title)
                                    <option value="{{ $abbreviation }}"{{$abbreviation == $license->state ? ' selected' : ''}}>{{ $title}}({{ $abbreviation }})</option>
                                @endforeach
                            </select>
                            <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                        </div>
                    </div>
                </div>

                <br/>

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <label for="expires_at">{{ _('Expiration Date') }}</label>
                        <div class="input-group">
                            <input type="text"
                                   id="expires_at"
                                   class="form-control date-picker"
                                   name="expires_at"
                                   placeholder="{{ _('Expiration Date') }}"
                                   value="{{ $license->expires_at->format('m/d/Y') }}">
                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <label for="license_image">{{ _('Update License Image') }} ({{ _('optional') }})</label>
                        <div class="input-group">
                            <input type="file"
                                   id="license_image"
                                   class="form-control"
                                   name="license_image">
                            <span class="input-group-addon"><i class="fa fa-upload"></i></span>
                        </div>
                    </div>
                </div>

                <br/>

                <div class="row">
                    <div class="col-xs-12 col-md-4 col-md-offset-8">
                        <input type="submit" class="btn btn-flat btn-sm btn-success btn-block" value="{{ _('Update License') }}">
                    </div>
                </div>
            </form>

        </div>

    </div>
</div>