@extends('admin.template')

@section('page-title', config('app.name'))

@section('page-js')
    <script src="{{ url('resources/bower/tinymce/tinymce.js') }}"></script>
    <script src="{{ url('resources/admin/js/content.js') }}"></script>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-files-o"></i> {{ _('Content') }}
                <small>{{ _('Edit content segment') }}</small>
            </h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> {{ _('Admin') }}</li>
                <li><i class="fa fa-files-o"></i> {{ _('Content') }}</li>
                <li class="active"><i class="fa fa-file-o"></i> {{ ucwords(str_replace('-', ' ', $uri)) }}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!--Main Row-->
            <div class="row">

                <!--Main Column-->
                <div class="col-xs-12">

                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('success') }}
                        </div>
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            @if($content->identifier == 'LICENSE_EXPIRATION_EMAIL')
                                <h3>{{ _('Email Variables') }}</h3>
                                <table class="table table-striped">
                                    <tr>
                                        <td><strong>{{ _('Variable') }}</strong></td>
                                        <td><strong>{{_('Description')}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>{USER_NAME}</td>
                                        <td>{{_('Insert User\'s Name')}}</td>
                                    </tr>
                                    <tr>
                                        <td>{USER_EMAIL}</td>
                                        <td>{{_('Insert User\'s Email Address')}}</td>
                                    </tr>
                                    <tr>
                                        <td>{LICENSE_EXPIRATION_DATE}</td>
                                        <td>{{_('Insert license\'s Expiration Date')}}</td>
                                    </tr>
                                    <tr>
                                        <td>{LICENSE_STATE}</td>
                                        <td>{{_('insert License\'s State')}}</td>
                                    </tr>
                                </table>
                            @endif
                        </div>
                    </div>

                    <h3>{{ ucwords(str_replace('-', ' ', $uri)) }}</h3>
                    <form action="{{ url('admin/content/update/'.$content->identifier) }}"
                          method="post"
                          id="edit-content"
                          data-confirm-message="{{ _('Are you sure you\'re ready to submit?') }}"
                          data-confirm-button="{{ _('Yes, I\'m sure') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <textarea name="content" style="height: 450px;">{!! old('content', $content->content) !!}</textarea>
                        <br/>
                        <input type="submit" class="btn btn-flat btn-success pull-right" value="{{ _('Save Content') }}">
                    </form>

                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>
@endsection