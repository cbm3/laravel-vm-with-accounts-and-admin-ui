@if(empty($licenses))
    {{ _('No results found') }}
@else
    @foreach($licenses as $license)
        <tr>
            <td class="text-center">{{ $license->title }}</td>
            <td class="text-center">{{ $license->number }}</td>
            <td class="text-center">{{ $license->state }}</td>
            <td class="text-center">{{ $license->licensure_date->format('m/d/Y') }}</td>
            <td class="text-center">{{ $license->last_renewal_date->format('m/d/Y') }}</td>
            <td class="text-center">{{ $license->expires_at->format('m/d/Y') }}</td>
            <td class="text-center">{!! time() >= strtotime($license->expires_at) ? _('YES') : _('NO') !!}</td>
            <td class="text-center">
                <a href="{{ url('license/'.$license->id.'/review-image') }}"
                   class="btn btn-xs btn-warning review-license-image{{ $license->image_uid ? '' : ' disabled' }}"
                   data-toggle="tooltip"
                   data-original-title="{{ _('Review License Image') . ($license->image_uid ? '' : ' ('._('no image available').')') }}"><i class="fa fa-file-image-o"></i></a>
                <a href="{{ url('license/'.$license->id.'/edit') }}"
                   class="btn btn-xs btn-info review-license"
                   data-toggle="tooltip"
                   data-original-title="{{ _('Review License') }}">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{ url('license/'.$license->id.'/delete') }}"
                   class="btn btn-xs btn-danger delete-license"
                   data-toggle="tooltip"
                   data-confirm-message="{{ _('Are you sure you want to delete this license?') }}"
                   data-confirm-button="{{ _('Yes I\'m Sure') }}"
                   data-original-title="{{ _('Delete License') }}"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
    @endforeach
@endif