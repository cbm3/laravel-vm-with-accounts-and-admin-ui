<?php $authUser = Auth::user(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""  />
    <meta name="keywords" content="" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('page-title', config('app.name'))</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ url('resources/client/images/favicon.ico') }}">

    <!-- google web fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700italic,700,800,800italic|Playfair+Display:400,700,400italic,700italic,900,900italic|Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Oswald:400,300,700|Lora:400,400italic,700,700italic|Poiret+One|Dancing+Script:400,700|Montserrat:400,700|Slabo+27px|Courgette|Dosis:400,200,300,500,600,700,800|Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic|Raleway:400,100,100italic,200,200italic,300,300italic,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Bootstrap -->
    <link href="{{ url('resources/client/css/bootstrap/bootstrap.min.css') }}" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- ######### CSS STYLES ######### -->
    <link rel="stylesheet" href="{{ url('resources/client/css/reset.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('resources/client/css/style.css') }}" type="text/css" />

    <!-- font icons -->
    <link rel="stylesheet" href="{{ url('resources/client/css/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('resources/client/css/simpleline-icons/css/simple-line-icons.css') }}" media="screen" />
    <link rel="stylesheet" href="{{ url('resources/client/css/simpleline-icons/css/et-linefont/etlinefont.css') }}">

    <!-- animations -->
    <link href="{{ url('resources/client/css/simpleline-icons/js/animations/aos.css') }}" rel="stylesheet" type="text/css" media="all" />

    <!-- responsive devices styles -->
    <link rel="stylesheet" media="screen" href="{{ url('resources/client/css/responsive-leyouts.css') }}" type="text/css" />

    <!-- style switcher -->
    <link rel = "stylesheet" media = "screen" href="{{ url('resources/client/js/style-switcher/color-switcher.css') }}" />

    <!-- sweet alert -->
    <link rel="stylesheet" href="{{ url('resources/bower/sweetalert/dist/sweetalert.css') }}">

    <!-- mega navigation menu -->
    <link rel="stylesheet" href="{{ url('resources/client/js/megamenu/stylesheets/screen.css') }}">

    <!-- masterslider -->
    <link rel="stylesheet" href="{{ url('resources/client/js/masterslider/style/masterslider.css') }}" />
    <link href="{{ url('resources/client/js/masterslider/skins/default/style.css') }}" rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ url('resources/client/js/masterslider/style/style.css') }}">

    <!-- cubeportfolio -->
    <link rel="stylesheet" type="text/css" href="{{ url('resources/client/js/cubeportfolio/css/cubeportfolio.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('resources/client/js/dheffect/style.css') }}">

    <!-- Page CSS -->
    @yield('page-css')

</head>

<body>
<div class="site-wrapper">

    <div class="topbar bg-color2">
        <div class="row nopadding">
            <div class="container nopad">

                <div class="col-md-6 col-xs-4 nopadding">
                    <ul class="none two padding-top-bottom18">
                        {{--<li><h6 class="small3 margin-top1 font-weight4 margin-right2"><a href="#" class="sty4">Conatct Us</a></h6></li>--}}
                        {{--<li><h6 class="small3 margin-top1 font-weight4 margin-right2"><a href="#" class="sty4">Purchase Now!</a></h6></li>--}}
                    </ul>
                </div><!--end left-->

                <div class="col-md-6 col-xs-8 nopadding">
                    <ul class="none two right padding-top-bottom18">
                        <li><h6 class="opensans small2 margin-top1 margin-right2"><i class="fa fa-mobile iconsize1"></i>&nbsp; (01) 123-456-7890</h6></li>
                        <li><h6 class="small2 margin-top1 font-weight4 margin-right2"><a href="mailto:info@website.com" class="sty4"><i class="fa fa-envelope"></i>&nbsp; info@website.com</a></h6></li>
                        <li><a href="#"><i class="fa fa-facebook icon-border-gray size5" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter icon-border-gray size5" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram icon-border-gray size5" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus icon-border-gray size5" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-rss icon-border-gray size5" aria-hidden="true"></i></a></li>
                    </ul>
                </div><!--end right-->

            </div>
        </div>
    </div><!-- end top bar -->

    <nav class="header-section white pin-style">
        <div class="container">
            <div>
                <div class="mod-menu">
                    <div class="row">
                        <div class="col-md-4 col-sm-2"> <a href="{{ url('/') }}" title="" class="logo"> <img src="http://www.placehold.it/250x50" alt="logo" /> </a> </div>
                        <div class="col-md-8 col-sm-10 nopadding">
                            <div class="main-nav">
                                <ul class="nav navbar-nav top-nav">
                                    <li class="visible-xs menu-icon"> <a href="javascript:void(0)" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false"> <i aria-hidden="true" class="fa fa-bars"></i> </a> </li>
                                </ul>
                                <div id="menu" class="collapse">
                                    <ul class="nav navbar-nav">
                                        <li> <a href="{{ url('/') }}">{{ _('Home') }}</a> </li>
                                        <li> <a href="{{ url('/license-locations') }}">{{ _('Locations') }}</a> </li>
                                        <li> <a href="{{ url('/about-us') }}">{{ _('About Us') }}</a> </li>
                                        <li> <a href="{{ url('/contact-us') }}">{{ _('Contact Us') }}</a> </li>
                                        @if($authUser)
                                            <li> <a href="{{ url('account') }}">{{ _('My Account') }}</a> </li>
                                            <li> <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ _('Sign Out') }}</a> </li>
                                            <form action="{{ url('logout') }}" method="post" id="logout-form">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            </form>
                                        @else
                                            <li> <a href="{{ url('/login') }}">{{ _('Login') }}</a> </li>
                                            <li> <a href="{{ url('/register') }}">{{ _('Sign Up') }}</a> </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- end navigation -->

    <div class="clearfix"></div>

    @yield('content')

    <footer class="bg-color-black">
        <div class="container-fulid">
            <div class="row nopadding">

                <div class="container padding-top-bottom2">

                    <div class="col-md-4 marbo-resp">
                        <h6 class="roboto white caps font-weight4">About {{ config('app.name') }}</h6>
                        <hr class="style-three color1 min-quarter2">
                        <p class="clearfix margin-bottom2"></p>
                        <h6 class="roboto small white font-weight3 line-height3 less5 left opacity4">All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures.</h6>
                    </div><!-- end col -->

                    <div class="col-md-4">
                        <h6 class="roboto white caps font-weight4">Office Address</h6>
                        <hr class="style-three color1 min-quarter2">
                        <p class="clearfix margin-bottom2"></p>
                        <h6 class="roboto small white font-weight3 line-height2 margin-bottom2 opacity4">275 Elm Street Hickory, Trunk Road, Seattle, WA 28601</h6>
                        <h6 class="roboto small white font-weight3 margin-bottom1 opacity4">Phone: &nbsp;<strong class="font-weight4">+44 (0)445 435 4255</strong></h6>
                        <h6 class="roboto small white font-weight3 margin-bottom opacity4">Fax: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong class="font-weight4">+44 (0)445 435 4266</strong></h6>
                        <h6 class="roboto small white font-weight3 margin-bottom1 opacity4">Email: &nbsp;&nbsp;&nbsp;<strong class="font-weight4"><a href="mailto:info@construct.com" class="white-opacity">info@construct.com</a></strong></h6>
                        <h6 class="roboto small white font-weight3 opacity4">Jobs: &nbsp;&nbsp;&nbsp;&nbsp;<strong class="font-weight4"><a href="mailto:careersconstruct.com" class="white-opacity">careers@construct.com</a></strong></h6>
                    </div><!-- end col -->


                </div>

                <div class="container-fulid bg-color-zblack text-center padding-top3 padding-bottom2">
                    <div class="container">
                        <div class="col-md-6 text-left"><h6 class="roboto small3 white font-weight3 opacity4 margin-top1">Copyright &copy; {{ date('Y', time()) }} {{ config('app.name') }}. All Rights Reserved.</h6></div>
                        <div class="col-md-6 text-right">
                            <a href="{{ url('privacy-policy') }}" target="_blank">{{ _('Privacy Policy') }}</a> |
                            <a href="{{ url('terms-of-use') }}" target="_blank">{{ _('Terms Of Use') }}</a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </footer><!-- end footer -->

    <a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

<!-- small modal -->
<div class="modal fade" id="sm-modal" tabindex="-1" role="dialog" aria-labelledby="small-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>
<!-- small modal -->

<!-- large modal -->
<div class="modal fade" id="lg-modal" tabindex="-1" role="dialog" aria-labelledby="large-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>
<!-- large modal -->

<!-- default modal -->
<div class="modal fade" id="md-modal" tabindex="-1" role="dialog" aria-labelledby="default-modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>
<!-- default modal -->

<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="{{ url('resources/client/js/universal/jquery.js') }}"></script>
<script src="{{ url('resources/client/js/style-switcher/styleselector.js') }}"></script>
<script src="{{ url('resources/client/js/animations/aos.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/client/js/bootstrap/bootstrap.min.js') }}"></script>

<script src="{{ url('resources/client/js/megamenu/js/main.js') }}"></script>

<script src="{{ url('resources/client/js/masterslider/masterslider.min.js') }}"></script>
<script type="text/javascript">
    var slider = new MasterSlider();

    // adds Arrows navigation control to the slider.
    slider.control('arrows');
    // slider.control('bullets');

    slider.setup('masterslider' , {
        width:1550,    // slider standard width
        height:670,   // slider standard height
        space:0,
        layout:'fullwidth',
        loop:true,
        preload:0,
        speed: 36,
        view:'parallaxMask',
        autoplay:true
    });
</script>

<script src="{{ url('resources/client/js/scrolltotop/totop.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/client/js/masterslider/tab.js') }}"></script>
<script type="text/javascript" src="{{ url('resources/client/js/cubeportfolio/js/jquery.cubeportfolio.min.js') }}"></script>
<script type="text/javascript" src="{{ url('resources/client/js/cubeportfolio/js/main.js') }}"></script>
<script src="{{ url('resources/client/js/parallax/parallax.js') }}"></script>
<script src="{{ url('resources/client/js/hslider/jquery.scrollslider.js') }}"></script>
<script src="{{ url('resources/bower/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ url('resources/client/js/numbers/incrementalNumber.js') }}"> </script>
<script type="text/javascript">
    $(document).ready(function(){
        incrementalNumber();
    });
</script>
<script type="text/javascript" src="{{ url('resources/client/js/dheffect/jquery.hoverdir.js') }}"></script>
<script type="text/javascript" src="{{ url('resources/client/js/dheffect/modernizr.custom.97074.js') }}"></script>
<script type="text/javascript">
    $(function() {

        $(' #da-thumbs > li ').each( function() { $(this).hoverdir(); } );

    });
</script>

<script type="text/javascript" src="{{ url('resources/client/js/universal/custom.js') }}"></script>
<!-- Apply CSFR Token To All AJAX Post Requests -->
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@yield('page-js')


</body>
</html>
