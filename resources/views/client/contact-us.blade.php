@extends('client.template')

@section('content')
    <br/>
    <div class="container">

        <section id="contact">
            <div class="contact-section">
                <div class="container">

                    <div class="row">
                        <div class="col-xs-12">

                            <h2>{{ _('Contact Us') }}</h2>
                            <hr/>

                            @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    {{ session()->get('success') }}
                                </div>
                            @endif

                            @if (count($errors) > 0)
                                <div class="row">
                                    @foreach ($errors->all() as $error)
                                        <div class="col-xs-12 col-md-6">
                                            <div class="alert alert-warning alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                {{ $error }}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>

                    <form action="{{ url('contact-us') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-6 form-line">
                            <div class="form-group">
                                <label for="exampleInputUsername">{{ strtoupper(_('Your Name')) }}</label>
                                <input type="text" class="form-control" id="" placeholder=" Enter Name" name="name" value="{{old('name', ($authUser ? $authUser->name : ''))}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail">{{ strtoupper(_('Email Address')) }}</label>
                                <input type="email" class="form-control" id="exampleInputEmail" placeholder="Enter Email Address"  name="email" value="{{old('email', ($authUser ? $authUser->email : ''))}}">
                            </div>
                            <div class="form-group">
                                <label for="telephone">{{ strtoupper(_('Mobile Number')) }}</label>
                                <input type="tel" class="form-control" id="telephone" placeholder="Mobile Number"  name="phone-number" value="{{old('phone-number', ($authUser ? $authUser->contact->phone_number : ''))}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for ="description"> {{ strtoupper(_('Message')) }}</label>
                                <textarea  class="form-control" id="description" placeholder="Enter Your Message" style="height: 180px;" name="message"></textarea>
                            </div>
                            <div>

                                <button type="submit" class="btn btn-default submit pull-right"><i class="fa fa-paper-plane" aria-hidden="true"></i>  {{ _('Send Message') }}</button>
                            </div>

                        </div>
                    </form>
                </div>
        </section>
    </div>
    <br/>
    <br/>
@endsection