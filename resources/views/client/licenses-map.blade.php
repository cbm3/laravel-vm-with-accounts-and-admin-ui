@extends('client.template')

@section('page-js')
    <script src="{{ url('resources/client/js/us-map-master/lib/raphael.js') }}"></script>
    <script src="{{ url('resources/client/js/us-map-master/jquery.usmap.js') }}"></script>
    <script>
        var states = {
            @foreach(config('regions.us-states') as $abbreviation => $name)
            '{{$abbreviation}}' : '{{ $name }}',
            @endforeach
        };

        var availableStates = [
            @foreach($states as $state)
            '{{ $state->abbreviation }}',
            @endforeach
        ];

        $('#license-map').usmap({
            'showLabels': true,
            'stateSpecificStyles': {
                @foreach($states as $state)
                '{{$state->abbreviation}}' : {fill: '#309b0f'},
                @endforeach
            },
            'stateSpecificHoverStyles': {
                @foreach($states as $state)
                '{{$state->abbreviation}}' : {fill: '#0d4a0d'},
                @endforeach
            },
            'stateStyles': {
                fill: '#000',
                "stroke-width": 1,
                'stroke' : '#333'
            },
            'stateHoverStyles': {
                fill: '#333'
            },
            'click' : function(event, data) {
                if ($.inArray(data.name, availableStates) !== -1) {
                    swal({
                        title: 'Yay!',
                        text:  'Yes, we have available contractors for the state of <strong>' + states[data.name] + '!</strong>',
                        html: true,
                        type: 'success'
                    });
                } else {
                    swal({
                        title: 'Check back soon!',
                        text:  'We currently don\'t have any available contractors for the state of <strong>' + states[data.name] + '!</strong>',
                        html: true,
                        type: 'warning'
                    });
                }
            }
        });
    </script>
@endsection

@section('content')
    <br/>
    <br/>
    <h6 class="text-center">{{ _('We have contractors available for all states highlighted in green!') }}</h6>
    <br/>
    <div class="container">
        <div id="license-map" style="width: 65%; height: 500px; margin: 0 auto;"></div>
    </div>
    <br/>
    <br/>
    @endsection