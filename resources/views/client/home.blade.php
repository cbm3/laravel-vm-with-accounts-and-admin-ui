@extends('client.template')

@section('content')
    <!-- slider -->
    <div>
        <div class="master-slider ms-skin-default" id="masterslider">

            <!-- new slide -->
            <div class="ms-slide slide-1 bg-color-zblack" data-delay="5">

                <!-- slide background -->
                <img src="js/masterslider/blank.gif" data-src="{{ url('resources/client/images/slider/01.jpg') }}" alt="" class="opacity4" />

                <h3 class="ms-layer roboto white caps font-weight4 hps-title7 letspacing3"
                    style="left: 200px; top: 200px;"
                    data-type="text"
                    data-delay="300"
                    data-duration="200"
                    data-effect="bottom(30)"
                    data-ease="easeOutExpo"
                >
                    Effective teamwork
                </h3>

                <h3 class="ms-layer roboto white caps font-weight8 hps-title1 letspacing2"
                    style="left: 200px; top: 245px;"
                    data-type="text"
                    data-delay="700"
                    data-duration="200"
                    data-effect="bottom(30)"
                    data-ease="easeOutExpo"
                >
                    Good teamwork <br /> makes efficient
                </h3>

            </div>
            <!-- end of slide -->

            <!-- new slide -->
            <div class="ms-slide slide-2 bg-color-zblack" data-delay="5">

                <!-- slide background -->
                <img src="js/masterslider/blank.gif" data-src="{{ url('resources/client/images/slider/02.jpg') }}" alt="" class="opacity4" />

                <h3 class="ms-layer roboto white caps font-weight4 hps-title7 letspacing3 center"
                    style="top: 200px;"
                    data-type="text"
                    data-delay="300"
                    data-duration="200"
                    data-effect="bottom(30)"
                    data-ease="easeOutExpo"
                >
                    Effective teamwork
                </h3>

                <h3 class="ms-layer roboto white caps font-weight8 hps-title1 letspacing2 center"
                    style="top: 245px;"
                    data-type="text"
                    data-delay="700"
                    data-duration="200"
                    data-effect="bottom(30)"
                    data-ease="easeOutExpo"
                >
                    We believe safety<br /> is always first
                </h3>

            </div>
            <!-- end of slide -->

        </div>
    </div>
    <!-- end of slider -->

    <div class="clearfix"></div>

    <section class="bg-color-darkgray padding-top-bottom2">
        <div class="container">
            <div class="row">

                <h2 class="roboto big1 white text-center font-weight3 line-height">{{ _('Let us help you with our years of experience.') }}</h2>

            </div>
        </div>
    </section><!-- end section -->

    <section class="background-image2 padding-top-bottom5 text-center">
        <div class="container">
            <div class="row">

                <h2 class="caps font-weight8 margin-bottom1">Our Services</h2>
                <h6 class="small font-weight4 line-height2 less3 margin-bottom2">We help you build things quickly and with efficiency.</h6>

                <p class="clearfix margin-bottom2"></p>

                <div class="col-md-3 col-sm-6 text-center marbo-resp aos-init" data-aos="fade-up" data-aos-delay="300" data-aos-duration="200">
                    <a href="#" class="none"><div class="padding4 hover-shadow-color5">
                            <i class="fa fa-cubes yellow center iconsize5" aria-hidden="true"></i>
                            <h5 class="roboto caps font-weight4 margin-top2 margin-bottom">General Contracting</h5>
                            <p>We need sure generate the tend true mobile book</p>
                        </div></a>
                </div><!-- end col -->

                <div class="col-md-3 col-sm-6 text-center marbo-resp aos-init" data-aos="fade-up" data-aos-delay="500" data-aos-duration="200">
                    <a href="#" class="none"><div class="padding4 hover-shadow-color5">
                            <i class="fa fa-lightbulb-o yellow center iconsize5" aria-hidden="true"></i>
                            <h5 class="roboto caps font-weight4 margin-top2 margin-bottom">Design and Building</h5>
                            <p>We need sure generate the tend true mobile book</p>
                        </div></a>
                </div><!-- end col -->

                <div class="col-md-3 col-sm-6 text-center marbo-resp14 aos-init" data-aos="fade-up" data-aos-delay="700" data-aos-duration="200">
                    <a href="#" class="none"><div class="padding4 hover-shadow-color5">
                            <i class="fa fa-clock-o yellow center iconsize5" aria-hidden="true"></i>
                            <h5 class="roboto caps font-weight4 margin-top2 margin-bottom">Construction Management</h5>
                            <p>We need sure generate the tend true mobile book</p>
                        </div></a>
                </div><!-- end col -->

                <div class="col-md-3 col-sm-6 text-center aos-init" data-aos="fade-up" data-aos-delay="900" data-aos-duration="200">
                    <a href="#" class="none"><div class="padding4 hover-shadow-color5">
                            <i class="fa fa-gavel yellow center iconsize5" aria-hidden="true"></i>
                            <h5 class="roboto caps font-weight4 margin-top2 margin-bottom">Pre construction services</h5>
                            <p>We need sure generate the tend true mobile book</p>
                        </div></a>
                </div><!-- end col -->

                <p class="clearfix margin-bottom3"></p>


            </div>
        </div>
    </section><!-- end section -->
    @endsection