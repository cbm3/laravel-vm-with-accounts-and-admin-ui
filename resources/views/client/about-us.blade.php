@extends('client.template')

@section('content')
    <br/>
    <div class="container">
        {!! $content->content !!}
    </div>
    <br/>
    <br/>
    @endsection