@extends('client.template')

@section('page-css')
<link rel="stylesheet" type="text/css" href="{{ url('resources/bower/bootstrap-datepicker/dist/css/bootstrap-datepicker.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ url('resources/bower/intl-tel-input/build/css/intlTelInput.css') }}"/>
@endsection

@section('page-js')
<script src="{{ url('resources/bower/intl-tel-input/build/js/intlTelInput.js') }}"></script>
<script src="{{ url('resources/bower/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
<script>
    $('.date-picker').datepicker();
    $('.phone-number-input').intlTelInput();
</script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="row">
                            {{--Left Form Column--}}
                            <div class="col-xs-12 col-md-6">

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-4 control-label">Name</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Password</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>

                            </div>
                            {{--Right Form Column--}}
                            <div class="col-xs-12 col-md-6">

                                <div class="form-group{{ $errors->has('address_line_1') ? ' has-error' : '' }}">
                                    <label for="address-line-1" class="col-md-4 control-label">Address</label>

                                    <div class="col-md-6">
                                        <input id="address-line-1" type="text" class="form-control" name="address_line_1" value="{{ old('address_line_1') }}" required>
                                        @if ($errors->has('address_line_1'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('address_line_1') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('address_line_2') ? ' has-error' : '' }}">
                                    <label for="address-line-2" class="col-md-4 control-label">Address Line 2</label>

                                    <div class="col-md-6">
                                        <input id="address-line-2" type="text" class="form-control" name="address_line_2" value="{{ old('address_line_2') }}">
                                        @if ($errors->has('address_line_2'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('address_line_2') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label for="city" class="col-md-4 control-label">City</label>

                                    <div class="col-md-6">
                                        <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}">
                                        @if ($errors->has('city'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                    <label for="state" class="col-md-4 control-label">State</label>

                                    <div class="col-md-6">
                                        <select name="state" class="form-control">
                                            @foreach(config('regions.us-states') as $abbreviation => $name)
                                                <option value="{{ $abbreviation }}"{{old('state') == $abbreviation ? ' selected' : ''}}>{{ $name }} ({{ $abbreviation }})</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('state'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
                                    <label for="zip" class="col-md-4 control-label">Zip Code</label>

                                    <div class="col-md-6">
                                        <input id="zip" type="text" class="form-control" name="zip" value="{{ old('zip') }}">
                                        @if ($errors->has('zip'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('zip') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                    <label for="phone_number" class="col-md-4 control-label">Phone Number</label>

                                    <div class="col-md-6">
                                        <input id="phone_number" type="text" class="form-control phone-number-input" name="phone_number" value="{{ old('phone_number') }}">
                                        @if ($errors->has('phone_number'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                                    <label for="birthday" class="col-md-4 control-label">Birthday</label>

                                    <div class="col-md-6">
                                        <input id="birthday" type="text" class="form-control date-picker" name="birthday" value="{{ old('birthday') }}">
                                        @if ($errors->has('birthday'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('birthday') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                                    <label for="phone_number" class="col-md-4 control-label">Company Name</label>

                                    <div class="col-md-6">
                                        <input id="company_name" type="text" class="form-control" name="company_name" value="{{ old('company_name') }}">
                                        @if ($errors->has('company_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('company_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('job_title') ? ' has-error' : '' }}">
                                    <label for="job_title" class="col-md-4 control-label">Job Title</label>

                                    <div class="col-md-6">
                                        <input id="job_title" type="text" class="form-control" name="job_title" value="{{ old('job_title') }}">
                                        @if ($errors->has('job_title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('job_title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-block btn-primary">
                                            Register
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
