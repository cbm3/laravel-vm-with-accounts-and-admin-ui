<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('address_line_1', 40);
            $table->string('address_line_2', 40)->nullable();
            $table->string('city', 40);
            $table->string('state', 2);
            $table->integer('zip')->length(10)->unsigned();
            $table->string('phone_number', 40);
            $table->timestamp('birthday');
            $table->string('company_name', 40);
            $table->string('job_title', 40);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
