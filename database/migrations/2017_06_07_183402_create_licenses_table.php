<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('description')->nullable();
            $table->string('state', 2);
            $table->string('title', 40);
            $table->string('number', 40)->unique();
            $table->string('trade', 40);
            $table->string('image_uid', 50)->nullable();
            $table->boolean('notified_of_expiration')->default(0);
            $table->dateTime('expires_at');
            $table->dateTime('licensure_date');
            $table->dateTime('last_renewal_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licenses');
    }
}