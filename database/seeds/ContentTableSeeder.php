<?php

use Illuminate\Database\Seeder;

class ContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Content::class, 1)->create()->each(function($content) {
            $content->identifier = 'ABOUT_US';
            $content->save();
        });

        factory(App\Content::class, 1)->create()->each(function($content) {
            $content->identifier = 'PRIVACY_POLICY';
            $content->save();
        });

        factory(App\Content::class, 1)->create()->each(function($content) {
            $content->identifier = 'TERMS_OF_USE';
            $content->save();
        });

        factory(App\Content::class, 1)->create()->each(function($content) {
            $content->identifier = 'LICENSE_EXPIRATION_EMAIL';
            $content->save();
        });
    }
}
