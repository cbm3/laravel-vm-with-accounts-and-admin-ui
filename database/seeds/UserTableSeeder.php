<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 50)->create()->each(function($user) {
            $user->licenses()->save(factory(App\License::class)->make());
            $user->licenses()->save(factory(App\License::class)->make());
            $user->licenses()->save(factory(App\License::class)->make());
            $user->licenses()->save(factory(App\License::class)->make());
            $user->contact()->save(factory(App\Contact::class)->make());
        });
    }
}
