<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'admin'             => rand(0,1),
        'name'              => $faker->name,
        'email'             => $faker->unique()->safeEmail,
        'password'          => bcrypt('password'),
        'remember_token'    => str_random(10),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Contact::class, function (Faker\Generator $faker) {
    return [
        'address_line_1'        => $faker->text(10),
        'address_line_2'        => $faker->text(10),
        'city'                  => $faker->text(5),
        'state'                 => array_keys(config('regions.us-states'))[rand(0,49)],
        'zip'                   => $faker->numberBetween(1111,4444),
        'phone_number'          => $faker->phoneNumber,
        'birthday'              => $faker->time('Y-m-d H:i:s', time()),
        'company_name'          => $faker->text(20),
        'job_title'             => $faker->text(15)
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\License::class, function (Faker\Generator $faker) {
    return [
        'description'       => $faker->text(200),
        'expires_at'        => date('Y-m-d H:i:s', strtotime('+1 year')),
        'state'             => array_keys(config('regions.us-states'))[rand(0,49)],
        'title'             => $faker->text(rand(30,40)),
        'number'            => $faker->text(rand(30,40)),
        'trade'             => [
            'AKadmin', 'AKjourn', 'Alarm',
            'Apprentice', 'Automotive',
            'Contractor', 'Electrical',
            'Engineer', 'HVACR',
            'Inspector', 'NonCourse',
            'Security'
        ][rand(0,11)],
        'licensure_date'    => date('Y-m-d H:i:s', strtotime('-1 year')),
        'last_renewal_date' => date('Y-m-d H:i:s', strtotime('-1 year'))
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Content::class, function (Faker\Generator $faker) {
    return [
        'content'       => $faker->text(200),
        'identifier'    => $faker->text(20)
    ];
});
