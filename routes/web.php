<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ClientController@index');
Route::get('about-us', 'ClientController@aboutUs');
Route::get('contact-us', 'ClientController@contactUs');
Route::post('contact-us', 'ClientController@sendContactEmail');
Route::get('privacy-policy', 'ClientController@privacyPolicy');
Route::get('terms-of-use', 'ClientController@termsOfUse');
Route::get('license-locations', 'ClientController@licenseLocations');

Route::group(['middleware' => ['auth','active']], function() {
    Route::get('account', 'UserController@account');
    Route::post('account/update', 'UserController@update');

    Route::group(['prefix' => 'license'], function() {
        Route::get('new', 'LicenseController@new');
        Route::post('create', 'LicenseController@create');
        Route::get('{license_id}/review-image', 'LicenseController@reviewImage')->where('license_id', '[0-9]+');
        Route::post('{license_id}/delete-image', 'LicenseController@deleteImage')->where('license_id', '[0-9]+');
        Route::get('{license_id}/edit', 'LicenseController@edit')->where('license_id', '[0-9]+');
        Route::post('{license_id}/update', 'LicenseController@update')->where('license_id', '[0-9]+');
        Route::post('{license_id}/delete', 'LicenseController@delete')->where('license_id', '[0-9]+');
        Route::get('list', 'LicenseController@list');
    });
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function() {

    Route::get('/', 'Admin\UserController@index');

    Route::group(['prefix' => 'user'], function() {
        Route::get('/', 'Admin\UserController@index');
        Route::post('deactivate/{user_id}', 'Admin\UserController@deactivate')->where('user_id', '[0-9]+');
        Route::post('activate/{user_id}', 'Admin\UserController@activate')->where('user_id', '[0-9]+');
        Route::get('list', 'Admin\UserController@list');
        Route::get('{user_id}', 'Admin\UserController@edit')->where('user_id', '[0-9]+');
        Route::post('{user_id}/update', 'Admin\UserController@update')->where('user_id', '[0-9]+');
    });

    Route::group(['prefix' => 'license'], function() {
        Route::get('new/{user_id}', 'Admin\LicenseController@new');
        Route::post('create/{user_id}', 'Admin\LicenseController@create')->where('user_id', '[0-9]+');
        Route::get('{license_id}/review-image', 'Admin\LicenseController@reviewImage')->where('license_id', '[0-9]+');
        Route::post('{license_id}/delete-image', 'Admin\LicenseController@deleteImage')->where('license_id', '[0-9]+');
        Route::get('{license_id}/edit', 'Admin\LicenseController@edit')->where('license_id', '[0-9]+');
        Route::post('{license_id}/update', 'Admin\LicenseController@update')->where('license_id', '[0-9]+');
        Route::post('{license_id}/delete', 'Admin\LicenseController@delete')->where('license_id', '[0-9]+');
        Route::get('list/{user_id}', 'Admin\LicenseController@list')->where('user_id', '[0-9]+');
    });

    Route::group(['prefix' => 'content'], function() {
        Route::post('update/{identifier}', 'Admin\ContentController@update');
        Route::get('{uri}', 'Admin\ContentController@edit');
    });

});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
